package services;

import converter.ProductConverter;
import dao.ProductDao;
import dto.ProductRequest;
import entity.Product;
import validator.ProductValidator;

import java.util.ArrayList;
import java.util.List;

public class ProductServiceImpl implements ProductService {

    private final Integer limitStock = 5;

    private final ProductDao productDao = new ProductDao() {
        @Override
        public void save(Product product) {

        }

        @Override
        public Product findById(Long id) {
            return null;
        }

        @Override
        public List<Product> findAll() {
            return null;
        }
    };

    public List<Product> notifyForLowStock(){
        List<Product> products = new ArrayList<>();
        List<Product> lowStock = new ArrayList<>();
        for (Product product : products) {
            if (product.getQuality() < limitStock){
                lowStock.add(product);
            }

        }
        return lowStock;

    }

    @Override
    public void createProduct(ProductRequest request) {
        Product product = ProductConverter.convertRequestToEntitiy(request);

        System.out.println(request);


    }
}
